﻿// Copyright(c) 2018 Axel Gembe<derago@gmail.com>
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

using System;
using System.Drawing;
using System.Windows.Forms;

namespace CoordinateHelper
{
    public partial class MainForm : Form
    {
        private static readonly Font _drawFont = new Font("Consolas", 16);
        private Bitmap _bitmap;

        public MainForm()
        {
            InitializeComponent();

            Icon = Properties.Resources.CoordinateHelper;

            StartPosition = FormStartPosition.Manual;
            Location = new Point(0, 0);
            Size = SystemInformation.VirtualScreen.Size;
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (_bitmap == null)
                _bitmap = new Bitmap(pictureBox.Width, pictureBox.Height);

            var screenLocation = pictureBox.PointToScreen(e.Location);
            using (var g = Graphics.FromImage(_bitmap)) {
                g.Clear(Color.Black);
                var str = $"{screenLocation.X}x{screenLocation.Y}";
                var strSize = g.MeasureString(str, _drawFont);

                g.DrawLine(Pens.Green, new Point(e.Location.X, 0), new Point(e.Location.X, pictureBox.Height));
                g.DrawLine(Pens.Green, new Point(0, e.Location.Y), new Point(pictureBox.Width, e.Location.Y));

                var drawLocation = e.Location;

                var moveUp = drawLocation.X + Math.Ceiling(strSize.Width) > SystemInformation.VirtualScreen.Width;
                if (moveUp) {
                    drawLocation.X = (int)(SystemInformation.VirtualScreen.Width - Math.Ceiling(strSize.Width));
                    drawLocation.X -= 20;
                } else {
                    drawLocation.X += 10;
                }

                var moveLeft = drawLocation.Y + Math.Ceiling(strSize.Height) > SystemInformation.VirtualScreen.Height;
                if (moveLeft) {
                    drawLocation.Y = (int)(SystemInformation.VirtualScreen.Height - Math.Ceiling(strSize.Height));
                    drawLocation.Y -= 20;
                } else {
                    drawLocation.Y += 10;
                }

                g.DrawString(str, _drawFont, Brushes.White, drawLocation.X, drawLocation.Y);
            }
            pictureBox.Image = _bitmap;
        }

        private void MainForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 27) // ESC
                Close();
        }
    }
}
